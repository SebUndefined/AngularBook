import { Component } from '@angular/core';
import {TodoModel} from "./todo.model";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  newTodoText: string;
  todoList: TodoModel[] = [];

  addTodo() {
    if(this.newTodoText) {
      console.log(this.todoList)
      this.todoList.push({
        text: this.newTodoText,
        isChecked: false
      });
      this.newTodoText = "";
    }
    else {
      alert("None");
    }
  }

  deleteTodo(todo: TodoModel) {
    let index = this.todoList.findIndex(t => {
      return t == todo;
    });
    if (index != -1) {
      this.todoList.splice(index,1);
    }
    else {
      alert("Index non trouvé");
    }
  }
}

