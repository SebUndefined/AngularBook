/**
 * Created by sebby on 17/08/17.
 */
import {TodoModel} from "./todo.model";
import {Component, Input} from "@angular/core";

@Component ({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @Input()
  todo: TodoModel;

}
