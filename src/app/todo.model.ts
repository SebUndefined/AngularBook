/**
 * Created by sebby on 17/08/17.
 */
export interface TodoModel {
  text: string;
  isChecked: boolean;
}
